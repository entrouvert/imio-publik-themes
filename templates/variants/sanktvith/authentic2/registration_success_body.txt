{% load i18n %}Hallo · Bonjour {{ user.get_full_name }} !

Ihre Registrierung auf {{ site }} war erfolgreich ! · Votre inscription sur {{ site }} a réussi !

Sie können sich anmelden auf · Vous pouvez vous connecter sur :

	{{ login_url }}
{% if user.username %}
	Nutzername · Nom d'utilisateur : {{ user.username %}{% endif %}
	E-Mail : {{ user.email }}{% if user.first_name %}
	Vorname · Prénom : {{ user.first_name }}{% endif %}{% if user.last_name %}
	Nachname · Nom : {{ user.last_name }}{% endif %}
