{% load i18n humanize %}{% with last_login_date=user.last_login|naturaltime %}Warnung · Attention : {{ user.get_full_name }} 
Ihre letzte Anmeldung war · votre dernière connexion était le : {{ last_login_date }}{% endwith %}
