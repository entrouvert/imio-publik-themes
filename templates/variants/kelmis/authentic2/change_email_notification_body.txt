{% load i18n %}{% if email_is_not_unique%}{% with name=user.get_short_name old_email=user.email %}Hallo · Bonjour {{ name }} !

Sie haben darum gebeten, Ihre E-Mail-Adresse auf {{ domain }} zu ändern von: · Vous avez demandé de modifier votre adresse e-mail sur {{ domaine }} à partir de :

  {{ old_email }}

 zu : · à :

  {{ email }}

Diese E-Mail ist jedoch bereits mit einem anderen Konto verknüpft. · Mais cet email est déjà lié à un autre compte.

Sie können dieses Kontopasswort mithilfe des Formulars zum Zurücksetzen des Passworts wiederherstellen : · Vous pouvez récupérer ce mot de passe de compte en utilisant le formulaire de réinitialisation de mot de passe :

  {{ password_reset_url }}

--
{{ domain }}{% endwith %}{% else %}{% with name=user.get_short_name old_email=user.email %}Hallo · Bonjour {{ name }} !

Sie haben darum gebeten, Ihre E-Mail-Adresse auf {{ domain }} von zu ändern · Vous avez demandé de modifier votre adresse e-mail sur {{ domaine }} à partir de :


  {{ old_email }}

zu : · à :

  {{ email }}

Um diese Änderung zu bestätigen, klicken Sie bitte auf den folgenden Link · Pour valider ce changement veuillez cliquer sur le lien suivant :

  {{ link }}

Dieser Link ist gültig für · Ce lien sera valable pour : {{ token_lifetime }}.

--
{{ domain }}{% endwith %}{% endif %}
