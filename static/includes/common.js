$(function() {
  // iMio common JS code for all themes
  const errorNotice = document.querySelector('.errornotice[role="status"]');

  if (errorNotice) {
      const errorMessage = errorNotice.querySelector('p');
      if (errorMessage && errorMessage.textContent === "Vous devez vous connecter avec une authentification plus forte pour accéder à ce formulaire.") {
          // Add the additional message after the first paragraph
          const additionalMessage = document.createElement('p');
          additionalMessage.innerHTML = '<strong>Vous êtes déjà connecté·e ?</strong> Rendez-vous dans <i>Mon compte</i> · <i>Carte eID</i> · <i>Lier mon compte à mon eID</i>, puis revenez compléter cette démarche.';
          errorNotice.appendChild(additionalMessage);
      }
  }
});
