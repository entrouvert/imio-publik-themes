$(function() {
    $.getScript('/static/imio/common.js', function() {});
    $.getScript('/static/includes/common.js', function() {});
    // Skin basket page. Split item name and item number
    var $basket_name_item = $('.lingobasketcell label > a');
        $basket_name_item.each(function() {
        var item = $(this).html();
        var $href = $(this).attr('href');
        var $item_name = item.split(" - n°")[0];
        var $item_num = item.split(" - n°")[1];
        $(this).html($item_name);
        if ($item_num) {
          $(this).after("<a class='basket-number' href='" + $href + "'>n° " + $item_num + "</a>");
        }
        });
    // End of Skin basket ...

    // old value : 'button[value="Acte de naissance pour un autre enfant."]'
    var naissance_button = $('button[value="Commander un autre acte de naissance"]');
    naissance_button.each(function() {	    
        var div_cible = $('div#receipt-intro > div');
        var screen_width = $(window).width();
        var naissance_form_resubmit = $('#wf-actions');
        naissance_form_resubmit.prependTo(div_cible);
        $(this).css('height','3.6em');
        $(this).css('padding','0em');
        $(this).css('padding-left','0.5em');
        $(this).css('padding-right','0.5em');
        $(this).css('background-color', '#BE0D67');
        $(this).css('color', 'white');
        $(this).css('border', 'solid #BE0D67 1px');
        $(this).css('border-radius', '10px');
        $(this).css('width','200px');
	$(this).css('text-transform','None');
        $('div#rub_service form').css('padding','0px');
        $(this).hover(function() {
            $(this).css("font-weight","normal");
        });
        if (screen_width > 960) {
            naissance_form_resubmit.css('position', 'absolute');
            $(this).css('margin-top','-1.9em');
            $('#workflow-button-paid').css('margin-left', '24%');
            $('#form_msg_top').css('height', '150px');
        }
        else {
            naissance_form_resubmit.css('position', 'relative');
            $(this).css('margin-top','0em');
            $('#workflow-button-paid').css('margin-left', '0em');
            $('#form_msg_top').css('height', '220px');
        }

        $(window).resize(function() {
            screen_width = $(window).width();
            if (screen_width > 960) {
               naissance_form_resubmit.css('position', 'absolute');
               $(this).css('margin-top','-1.9em');
               $('#workflow-button-paid').css('margin-left', '24%');
               $('#form_msg_top').css('height', '150px');
            }
            else {
                naissance_form_resubmit.css('position', 'relative');
                $(this).css('margin-top','0em');
                $('#workflow-button-paid').css('margin-left', '0em');
                $('#form_msg_top').css('height', '220px');
            }
        });
    });


    $(document).on('click', '.jq_txt_connexion', function() {
        window.open("login", '_self');
    });

    $(document).on('mouseover', '.jq_txt_connexion', function() {
        $('.jq_txt_connexion').css('cursor','pointer');
        $('.jq_txt_connexion b').css('color','#be0d67');
    });

    $(document).on('mouseout', '.jq_txt_connexion', function() {
        $('.jq_txt_connexion b').css('color','black');
    });

    // Timeout (10'') to automaticaly exit basket page when payment done
    var panier_exit = $('body.page-panier ul.messages > li.info:contains("Votre paiement a été pris en compte.")')
    panier_exit.each(function() {
        setTimeout(function() {
            window.top.location.href = "../"
        }, 10000);
    });

    // Multi attachments in forms "layout"
    // . Add some attachments fields one afther the other
    // . Add "multi_attachments" class to first one
    var multi_attachments = $('div.multi_attachments');
    var lst_remove_files = $('div.file-upload-widget > div.content > div.fileinfo > .remove');
    var lst_attachments_to_hide = multi_attachments.nextUntil("div:not(.file-upload-widget)");
    var cpt = 0;
    multi_attachments.on( "click", display_next_file_upload );
    lst_remove_files.on( "click", remove );
    lst_attachments_to_hide.each(function() {
        $(this).on("click", display_next_file_upload);
        if (cpt == 0) {
            $(this).find('input').attr('disabled','disabled');
        }
        else {
            $(this).css( "display", "none" );
        }
        cpt++;
    });

    function display_next_file_upload() {
        var next_file_widget = $(this).next();
        next_file_widget.find('input').removeAttr('disabled');
        next_file_widget.css( "display", "block" );
    }

    function remove() {
        var file_upload_widget = $(this).parent().parent().parent();/*parentsUntil('div.file-upload-widget')*/
        if (! file_upload_widget.hasClass( 'multi_attachments' )) {
            file_upload_widget.insertAfter(lst_attachments_to_hide[(lst_attachments_to_hide.length - 1)]);
            file_upload_widget.css( "display", "none" );
        }
        lst_attachments_to_hide = multi_attachments.nextUntil("div:not(.file-upload-widget)");
    }
/*
    // Print Alert when we quit a form or exit browser out from a form
    $(window).bind('beforeunload', exit_msg);
    var form_next = $('form.quixote button[name=submit]');
    var form_previous = $('form.quixote button[name=previous]');
    var form_cancel = $('form.quixote button[name=cancel]');
    var form_savedraft = $('form.quixote button[name=save-draft]');
    form_next.mouseout(function() {
        $(window).bind('beforeunload', exit_msg);
    });
    form_previous.mouseout(function() {
        $(window).bind('beforeunload', exit_msg);
    });
    form_cancel.mouseover(function() {
        $(window).bind('beforeunload', exit_msg);
    });
    form_next.mouseover(function() {
        $(window).unbind('beforeunload', exit_msg);
    });
    form_savedraft.mouseover(function() {
        $(window).unbind('beforeunload', exit_msg);
    });
    form_savedraft.click(function() {
        $(window).bind('beforeunload', exit_msg);
    });
    form_previous.mouseover(function() {
        $(window).unbind('beforeunload', exit_msg);
    });*/
});
    /* on unbind pas le beforeunload en quittant le bouton cancel
     car on ne sait pas la prochaine action du citoyen.
    form_cancel.mouseout(function() {
        $(window).unbind('beforeunload', exit_msg);
    });*/
    /*
var exit_msg = function(e) {
    if ($('form').attr('class') == 'quixote') {
        var confirmationMessage = 'Vous quittez votre formulaire. Etes-vous sur?';
        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    }
}*/
