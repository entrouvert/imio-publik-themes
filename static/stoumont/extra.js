$(function() {
  $.getScript('/static/includes/common.js', function() {});
  /* unite all errors to the top of the form */
  var $error_notice = $('form.quixote > div.errornotice');
  var $errors = $('form.quixote div.error');
  if ($errors.length && $error_notice.length == 0) {
    $error_notice = $('<div class="errornotice"></div>');
    $('form.quixote').prepend($error_notice);
  }
  $errors.each(function() {
    var field_label = $(this).parents('.widget').find('.title label').text().replace(/[ :\*]+$/g, '');
    var $error_text = $('<p>' + field_label + ' : ' + $(this).text() + '</p>');
    $(this).hide();
    $error_notice.append($error_text);
  });

  /*Set readonly attributes on "input, textarea and select" element when meeting a .readonly css class on div parent*/
  $form_divs_readonly = $('form.quixote div.readonly');
  $form_divs_readonly.each(function() {
   $("input", this).prop('readonly', true);
   $("textarea", this).prop('readonly', true);
   $("select", this).prop('readonly', true);
  });

  /*Remove "Belgique" from select box.*/
  $country_datasrc_without_belgium = $('.without_belgium select');
  $country_datasrc_without_belgium.each(function() {
   $(this).children('option[value="Belgique"]').remove();
  });

  /*
  If a form table have a class ".is_filtering_table" and on the same page, there is a textbox 
  with a table_filter css class
  SO this table could be filtering thanks to textbox text content... if "title" table row is in textbox text
  MANDATORY REQUIREMENT IN A PAGE FORM :
  .is_filtering_table on a table
  .table_filter on a textbox
  */
  $filtering_table = $('.is_filtering_table tbody > tr')
  $filtering_table.each(function() {
      $(this).hide()
      filtres = $('.table_filter > div.content > input').val().toLowerCase().split(',');
      if ($.inArray($(this).text().toLowerCase(), $.map(filtres, $.trim)) > -1)
      {
         $(this).show();
      }
  });

  $('form[data-keywords="pdf"]').each(function() {
    var $this_form = $(this);
    $(this).find('.cancel-button').hide();
    $(this).find('.submit-button button').attr('value', 'Télécharger');
    var text =  $(this).find('.submit-button button').text();
    $(this).find('.submit-button button').text(text.replace('Suivant','Télécharger'));
    $(this).on('submit', function() {
      window.location = $this_form.find('a').not('.ignore').attr('href');
      return false;
    });
  });

  /*City where first letter is uppercase*/
  $Header_title = $('h1#logo a')
  $Header_title.each(function() {
    var text = $(this).text();
    $(this).text(text.replace('oupeye','Oupeye'));
    return false;
  });


  /* textboxfield with var "txt_selected_motifs" + tablefield in the same page to manage multi motifs selection and number of copies.
     Table MUST have n lines for n motivations max.
     Table MUST have 2 columns "Motifs" keep motivation name and "Nb exemplaire" to input number of copies*/
  $table_nb_motifs = $('span#txt_selected_motifs').parent().parent().next();
  $table_nb_motifs.attr("id","var_table_nb_motifs");
  $table_nb_motifs = $('div#var_table_nb_motifs table')
  $table_nb_motifs.each(function() {
    $('span#txt_selected_motifs').hide();
    $lst_selected_motifs = $('span#txt_selected_motifs').text().split(', ');
    $tr = $('div#var_table_nb_motifs table > tbody > tr');
    $('div#var_table_nb_motifs table thead tr').find('th:nth-child(2)').toggle();
    $tr.each(function(tr){
      $name = $(this).find('input').attr('name');
      var new_motif_input_name = $name.split('-')[0] + '-' + tr + '-' + $name.split('-')[2];
      var new_nb_ex_input_name = $name.split('-')[0] + '-' + tr + '-' + (parseInt($name.split('-')[2]) + 1);
      var input_motif = "<input id=\"form_"+new_motif_input_name+"\" name=\""+new_motif_input_name+"\" type=\"text\" value=\""+$lst_selected_motifs[tr]+"\" />";
      var input_nb_ex = "<input id=\"form_"+new_nb_ex_input_name+"\" name=\""+new_nb_ex_input_name+"\" type=\"text\" />";
      if (tr < $lst_selected_motifs.length) {
        $(this).replaceWith("<tr><th style='text-align:left;'>"+$lst_selected_motifs[tr]+"</th><td style='display:none'>"+input_motif+"</td><td>"+input_nb_ex+"</td> </tr>");
      }
      else {
        $(this).remove();
      }
    });
  });
  /* END textboxfield with var "txt_selected_motifs" ... */

  /*Hide field's title*/
  $hide_title = $('.hide_title')
  $hide_title.each(function() {
    $(this).children('.title').hide();
  });

});
