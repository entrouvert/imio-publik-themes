# Script to add new scss @import in all style.scss files (except in static/imio folder)
# SAMPLE : python add_import_in_style.scss.py ../imio/cells.scss
# This line "../imio/cells.scss" will be adding in all commune folder
# Use git checkout to undo!
import os
import sys

root_folder = 'static'
fileSet = set()
value = "@import '{}';\n".format(sys.argv[1])
for folder, under_folders, files in os.walk(root_folder):
    for filename in files:
        if filename == 'style.scss' and folder != 'imio':
            relative_folder = os.path.relpath(folder, root_folder)
            relative_path_file = os.path.join(relative_folder, filename)
            with open('{}/{}'.format(root_folder, relative_path_file), 'r') as f_in_readonly:
                contents = f_in_readonly.readlines()
            if value not in contents:
                contents.insert(len(contents) - 1, value)
                with open('{}/{}'.format(root_folder, relative_path_file), 'w') as f_in_write:
                    contents = ''.join(contents)
                    f_in_write.write(contents)
