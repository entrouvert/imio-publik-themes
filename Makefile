VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="imio-publik-themes"

prefix = /usr

all: themes.json css

themes.json: $(wildcard static/*/config.json)
	python3 publik-base-theme/create_themes_json.py --overlay imio

%.css: export LC_ALL=C.UTF-8
.SECONDEXPANSION:
%.css: %.scss $$(wildcard $$(@D)/*.scss)
	sassc $< $@ --style compressed

publik-base-theme/static/includes/_data_uris.scss: $(wildcard publik-base-theme/static/includes/img/*)
	cd publik-base-theme; python3 make_data_uris.py static/includes/

css: publik-base-theme/static/includes/_data_uris.scss $(shell python3 get_themes.py)
	rm -rf static/*/.sass-cache/
	for inode2 in  $(shell ls -d templates/variants/*/) ; do \
		if [ -f $$inode2/prepare-template.py ]; then \
			cd $$inode2 && python3 prepare-template.py; \
			cd ../../..; \
		fi \
        done

clean:
	rm -rf sdist

DIST_FILES = \
	Makefile \
	publik-base-theme \
	static templates \
	get_themes.py

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/imio
	cp -r static templates themes.json $(DESTDIR)$(prefix)/share/publik/themes/imio
	rm $(DESTDIR)$(prefix)/share/publik/themes/imio/static/*/config.json

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

imio-test:
	pytest -qq --tb=no test_imio.py
